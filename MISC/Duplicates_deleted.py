import os
import hashlib 
from collections import Counter
File='MPCS_TXTA_PRD_20180524_073801_REBAT.txt'
path='D:\\work\\2018\\QIP_Dups_20180524'
dupfile=File.split('.')[0]+'_Dup.txt'
#varibales
lineinserted=[]
   #lineshash = dict(enumerate([ (hashlib.md5(line.strip()).hexdigest()) for line in rf],start=1)) -- ading index and converting a list to dictionary
with open(os.path.join(path,dupfile),'w') as wf:
    with open(os.path.join(path,File), 'r') as rf:
        lineshash = [ (hashlib.md5(line.strip()).hexdigest()) for line in rf]
        dups=[item for item,count  in Counter(lineshash).items() if count>1]
        rf.seek(0)
        for line in rf:
            linehval= hashlib.md5(line.strip()).hexdigest()
            if linehval in set(dups) and linehval not in set(lineinserted):
                lineinserted.append(linehval)
                wf.write(line)
            else:
                continue