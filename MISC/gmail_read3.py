
import smtplib
import time
import imaplib
import email
import pandas as pd
import os
from coalesce import coalesce
import re

def get_decoded_email_body(message_body):
    raw_email_string = message_body.decode('utf-8')
# converts byte literal to string removing b''
    email_message = email.message_from_string(raw_email_string)
# this will loop through all the available multiparts in mail
    #print(email_message)
    for part in email_message.walk():
       # print(part)
        if part.get_content_type() != "text/plain": # ignore attachments/html
            body = part.get_payload(decode=True)
            #save_string = str("D:Dumpgmailemail_" + str(x) + ".eml")
    return body.decode('utf-8')



def parse_html(body):
    from bs4 import BeautifulSoup
    import re
    import urllib

    #print('html')
   # print(body)
    p=re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
    Csoup = BeautifulSoup(body, 'html.parser')
    cust_count=1
    Menu_details=''
    order_no_in_next_line=0
    
    for cust_ns in Csoup.find_all('div', {'style':"margin:10px"}):
        for cust_name in cust_ns.find_all('div',{'style':re.compile('^color.*')}):
            if cust_count==2:
                customer=cust_name.text.strip()
                
            cust_count+=1
    for order_det in Csoup.find_all('div', {'data-section':re.compile('order')}):
        scheduled_dt=order_det.find('div', {'data-field':re.compile('scheduled-dt')}).text.strip().replace('T',' ').replace('Z','')
        estimated_dt=order_det.find('div', {'data-field':re.compile('estimated-dt')}).text.strip().replace('T',' ').replace('Z','')
        confirmation_number=order_det.find('div', {'data-field':re.compile('confirmation-number')}).text.strip()
        service_type=Csoup.find('div', {'data-field':re.compile('service-type')}).text.strip()
        
    for address in Csoup.find_all('div', {'data-section':re.compile('diner')}):
        phone=address.find('div', {'data-field':re.compile('phone')}).text.strip()
        if service_type=='Delivery':            
            address1=address.find('div', {'data-field':re.compile('address1')}).text.strip()
            address2=address.find('div', {'data-field':re.compile('address2')}).text.strip()
            city=address.find('div', {'data-field':re.compile('city')}).text.strip()
            state=address.find('div', {'data-field':re.compile('state')}).text.strip()
            zipc=address.find('div', {'data-field':re.compile('zip')}).text.strip()
        else:
            address1=''
            address2=''
            city=''
            state=''
            zipc=''
        
   
    for menu_det in Csoup.find_all('div', {'data-section':re.compile('menu-item')}):       
        
            Menu_details+=menu_det.find('div', {'data-field':re.compile('menu-item-name')}).text.strip()+','

    for order_no_search in Csoup.find_all('div',{'style':re.compile('^color.*')}):
        for order_no in order_no_search.find_all('span'):
           
            if order_no.text.strip()!='Order:' and order_no_in_next_line==0:
              
                continue
            elif order_no.text.strip()=='Order:':
                
                order_no_in_next_line=1
                continue
            elif  order_no_in_next_line==1:
                
                order_number= order_no.text.strip()
                order_no_in_next_line=0
                break
    print('order_no:'+order_number)
    cust_addr=address1+address2+city+state+zipc
    '''
    print('order_type:'+service_type) 
    print('order_no:'+order_number)  
    print('ordered_date:'+scheduled_dt)  
    print('cust_name:'+customer)  
    print('cust_addr:'+cust_addr)
    print('ordered_items:'+Menu_details)
    print('phone:'+phone)'''
    
    return service_type,order_number,scheduled_dt,Menu_details,customer,cust_addr,phone
   
                
                
            
        



               




def Extract_Orders_4m_mail(SMTP_SERVER,FROM_EMAIL,FROM_PWD,LABEL,SENDER,SUBJECT_SEARCH ):
    #try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER,PORT)
        mail.login(FROM_EMAIL, FROM_PWD)
        mail.select(LABEL)

        type, data = mail.search(None, '(FROM "%s" SUBJECT "%s")' %(SENDER,SUBJECT_SEARCH) )
        mail_ids = data[0]
    
     
        id_list = mail_ids.split()
        
        order_type=[]
        order_no=[]
        order_date=[]
        order_details=[]
        item_no=[]
        cust_name=[]
        cust_addr=[]
        cust_num=[]
        order_dict={}
        o_dict={}
        count_set=10000
        for i in reversed(id_list):
            if count_set>6736:
                print(count_set)
                count_set-=1
				
            else:
                break
           
            typ, data = mail.fetch(i, '(RFC822)')
            pattern=re.compile('.*Confirmation')
            for response_part in data:
                
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1].decode('utf-8'))
                    email_subject = msg['subject']
                    email_from = msg['from']
#                     print('From : ' + email_from + '\n')
#                     print('Subject : ' + email_subject + '\n')
#                     print('date : ' + msg['date'] + '\n')
                    if pattern.match(email_subject):

                        html_code=get_decoded_email_body (response_part[1])
                        ordertype,orderno,orderdate,orderdetails,custname,custaddr,custnum=parse_html(html_code)

                        order_type.append(ordertype)
                        order_no.append(orderno)
                        order_date.append(orderdate)
                        iph=orderdetails.split(',')[0].split('.')
                        if len(iph)>1:
                            order_details.append(iph[1])
                            item_no.append(iph[0])
                        else:
                            order_details.append(iph[0])
                            item_no.append('')						
                        cust_name.append(custname)
                        cust_addr.append(custaddr)
                        cust_num.append(custnum)
                        for i in range(len(orderdetails.split(','))):
                            if i==0 or i==len(orderdetails.split(','))-1:
                                continue
                            order_type.append('')
                            order_no.append(orderno)
                            order_date.append('')
                            iph=orderdetails.split(',')[i].split('.')
                            if len(iph)>1:
                                order_details.append(iph[1])
                                item_no.append(iph[0])
                            else:
                                order_details.append(iph[0])
                                item_no.append('')
                            cust_name.append('')
                            cust_addr.append('')
                            cust_num.append('')

        return{'ORDER_TYPE':order_type,'ORDER_NO':order_no,'ORDER_DATE':order_date,'ITEM_NO':item_no,'ITEM_DESC':order_details,'CUSTOMER_NAME':cust_name,'CUSTOMER_ADDR':cust_addr,'CUSTOMER_PHONE':cust_num}


#Enter gmail uid and pwd along with sender ,subject_search
print("Enter your gmail id ex:'myid@gmail.com'")
EMAIL  = input()
print("Enter your GMAIL password")
PWD    = input()
SERVER = "imap.gmail.com"
print("Enter the sender name you are looking for ex:'James Bond'")
SENDER= input()
print("Enter the search string in SUBJECT  you are looking for ex:'Orders'")
SUBJECT_SEARCH=input()
FROM_LABEL='INBOX'
PORT   = 993
path="D:\\"
file="Customer_order_details.xls"
ff=os.path.join(path, file)





ExWriter = pd.ExcelWriter(ff)
dic1=Extract_Orders_4m_mail(SERVER,EMAIL,PWD,FROM_LABEL,SENDER,SUBJECT_SEARCH)
#print(dic1)
df_CustData=pd.DataFrame(dic1)
df_CustData.to_excel(ExWriter,'sheet1',index=False,)

ExWriter.save()
print(" here is your file '%s'"%(ff))
