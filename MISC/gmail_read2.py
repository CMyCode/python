
import smtplib
import time
import imaplib
import email
import pandas as pd
import os



def get_decoded_email_body(message_body):
    raw_email_string = message_body.decode('utf-8')
# converts byte literal to string removing b''
    email_message = email.message_from_string(raw_email_string)
# this will loop through all the available multiparts in mail
    #print(email_message)
    for part in email_message.walk():
       # print(part)
        if part.get_content_type() != "text/plain": # ignore attachments/html
            body = part.get_payload(decode=True)
            #save_string = str("D:Dumpgmailemail_" + str(x) + ".eml")
    print(body.decode('utf-8'))
    return body.decode('utf-8')


def parse_html(body):
    from bs4 import BeautifulSoup
    import re

    p=re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
    Csoup = BeautifulSoup(body, 'html.parser')
    for order in Csoup.find_all('div', {'id':re.compile('.*cust_service_info')}):
        
        order=list(order.find_all(text=True))
        order_det=order[2].split(' ')
        order_no=order_det[1] 
        if len(order_det)==3:
            order_type=order_det[2]
        else:
            order_type='Delivery'
			
        cust_info_collect=0
        order_info_collect=0
        cust_info_ilne_count=0
        cust_addr=''
        order_details=''
        order_dict={}
    for j in Csoup.find_all('tr'):
        cols =j.find_all('td')
        vals=([ele.text.strip() for ele in cols])
        #print(vals)
        if vals[0]=='' :
            continue
        
        if vals[0] in ('Prepare for:','Deliver to:') :
            #print(1)
            cust_info_collect=1
            continue
        if vals[0]=='Qty':
            #print(2)
            order_info_collect=1
            cust_info_collect=2
            continue
        if cust_info_collect==1 and order_info_collect==0: 
            #print(3)
            if cust_info_ilne_count==0:
                #print(4)
                cust_name=vals[0]
                order_date=vals[1]
                cust_info_ilne_count=1
            else:
                if(re.match(p,vals[0]) ):
                    cust_num=vals[0]
                else:
                    if len(vals)>=2 :
                        order_date+=vals[1]
                    cust_addr+=vals[0]
        if vals[0]=='Subtotal':
            #print(6)
            order_info_collect=2
            continue
        if order_info_collect==1:
            #print(7)
            order_details+=vals[1]+','
            order_dict[vals[1]]=order_no
    '''
    print('order_type:'+order_type) 
    print('order_no:'+order_no)  
    print('ordered_date:'+order_date)  
    print('cust_name:'+cust_name)  
    print('cust_addr:'+cust_addr)
    print('ordered_items:'+order_details)
    print('phone:'+cust_num)'''
    return(order_type,order_no,order_date,order_details,cust_name,cust_addr,cust_num)
    #return(order_type)




def Extract_Orders_4m_mail(SMTP_SERVER,FROM_EMAIL,FROM_PWD,LABEL,SENDER,SUBJECT_SEARCH ):
    #try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER,PORT)
        mail.login(FROM_EMAIL, FROM_PWD)
        mail.select(LABEL)

        type, data = mail.search(None, '( SINCE "01-Jan-2017" BEFORE "31-dec-2017" )' )
        mail_ids = data[0]
     
        id_list = mail_ids.split()
        
        order_type=[]
        order_no=[]
        order_date=[]
        order_details=[]
        item_no=[]
        cust_name=[]
        cust_addr=[]
        cust_num=[]
        order_dict={}
        o_dict={}
        for i in reversed(id_list):
           
            typ, data = mail.fetch(i, '(RFC822)')
        
            for response_part in data:
                
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1].decode('utf-8'))
                    email_subject = msg['subject']
                    email_from = msg['from']
#                     print('From : ' + email_from + '\n')
#                     print('Subject : ' + email_subject + '\n')
#                     print('date : ' + msg['date'] + '\n')
                    html_code=get_decoded_email_body (response_part[1])
                    wf.write(html_code)
                    ordertype,orderno,orderdate,orderdetails,custname,custaddr,custnum=parse_html(html_code)
                    
                    order_type.append(ordertype)
                    order_no.append(orderno)
                    order_date.append(orderdate)
                    iph=orderdetails.split(',')[0].split('.')
                    order_details.append(iph[1])
                    item_no.append(iph[0])
                    cust_name.append(custname)
                    cust_addr.append(custaddr)
                    cust_num.append(custnum)
                    for i in range(len(orderdetails.split(','))):
                        if i==0 or i==len(orderdetails.split(','))-1:
                            continue
                        order_type.append('')
                        order_no.append('')
                        order_date.append('')
                        iph=orderdetails.split(',')[i].split('.')
                        order_details.append(iph[1])
                        item_no.append(iph[0])
                        cust_name.append('')
                        cust_addr.append('')
                        cust_num.append('')

        return{'ORDER_TYPE':order_type,'ORDER_NO':order_no,'ORDER_DATE':order_date,'ITEM_NO':item_no,'ITEM_DESC':order_details,'CUSTOMER_NAME':cust_name,'CUSTOMER_ADDR':cust_addr,'CUSTOMER_PHONE':cust_num}


#Enter gmail uid and pwd along with sender ,subject_search
print("Enter your gmail id ex:'myid@gmail.com'")
EMAIL  = input()
print("Enter your GMAIL password")
PWD    = input()
SERVER = "imap.gmail.com"
print("Enter the sender name you are looking for ex:'James Bond'")
SENDER= input()
print("Enter the search string in SUBJECT  you are looking for ex:'Orders'")
SUBJECT_SEARCH=input()
FROM_LABEL='INBOX'
PORT   = 993
path="D:\\"
file="Customer_order_details.xls"
file1="Customer_order_details.html"
ff=os.path.join(path, file)

wf=open(os.path.join(path,file1),'w')  



ExWriter = pd.ExcelWriter(ff)
dic1=Extract_Orders_4m_mail(SERVER,EMAIL,PWD,FROM_LABEL,SENDER,SUBJECT_SEARCH)
df_CustData=pd.DataFrame(dic1)
df_CustData.to_excel(ExWriter,'sheet1',index=False,)

ExWriter.save()
print(" here is your file '%s'"%(ff))